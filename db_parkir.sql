-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 21, 2017 at 11:05 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_parkir`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id_admin` char(3) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `telp` varchar(12) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `alamat`, `telp`, `username`, `password`) VALUES
('101', 'Angga Fauzy', 'Cihampelas', '082322334455', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `kendaraan`
--

CREATE TABLE IF NOT EXISTS `kendaraan` (
  `kode_jenis` char(3) NOT NULL,
  `jenis` varchar(10) NOT NULL,
  `tarif_parkir` varchar(10) NOT NULL,
  `tarif_perjam` varchar(10) NOT NULL,
  `denda` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kendaraan`
--

INSERT INTO `kendaraan` (`kode_jenis`, `jenis`, `tarif_parkir`, `tarif_perjam`, `denda`) VALUES
('MBL', 'Mobil', '5000', '3000', '100000'),
('MTR', 'Motor', '2000', '1000', '50000');

-- --------------------------------------------------------

--
-- Table structure for table `tb_parkir`
--

CREATE TABLE IF NOT EXISTS `tb_parkir` (
  `no_tiket` varchar(13) NOT NULL,
  `tgl_masuk` varchar(10) NOT NULL,
  `tgl_keluar` varchar(10) NOT NULL,
  `nama_admin` varchar(30) NOT NULL,
  `plat_nomor` varchar(10) NOT NULL,
  `kode_jenis` char(3) NOT NULL,
  `jam_masuk` varchar(10) NOT NULL,
  `jam_keluar` varchar(10) NOT NULL,
  `lama_parkir` varchar(10) NOT NULL,
  `biaya_parkir` varchar(10) NOT NULL,
  `total_biaya` varchar(10) NOT NULL,
  `sudah_keluar` varchar(10) NOT NULL,
  `ada_struk` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pkrkeluar`
--

CREATE TABLE IF NOT EXISTS `tb_pkrkeluar` (
  `no_tiket` varchar(13) NOT NULL,
  `tgl_masuk` varchar(10) NOT NULL,
  `tgl_keluar` varchar(10) NOT NULL,
  `nama_admin` varchar(30) NOT NULL,
  `plat_nomor` varchar(10) NOT NULL,
  `kode_jenis` char(3) NOT NULL,
  `jam_masuk` varchar(10) NOT NULL,
  `jam_keluar` varchar(10) NOT NULL,
  `lama_parkir` varchar(10) NOT NULL,
  `biaya_parkir` varchar(10) NOT NULL,
  `total_biaya` varchar(10) NOT NULL,
  `sudah_keluar` varchar(10) NOT NULL,
  `ada_struk` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_pkrmasuk`
--

CREATE TABLE IF NOT EXISTS `tb_pkrmasuk` (
  `no_tiket` varchar(13) NOT NULL,
  `tgl_masuk` varchar(10) NOT NULL,
  `nama_admin` varchar(30) NOT NULL,
  `plat_nomor` varchar(10) NOT NULL,
  `kode_jenis` char(3) NOT NULL,
  `jam_masuk` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pkrmasuk`
--

INSERT INTO `tb_pkrmasuk` (`no_tiket`, `tgl_masuk`, `nama_admin`, `plat_nomor`, `kode_jenis`, `jam_masuk`) VALUES
('20170521001', '10/05/2017', 'Angga Fauzy', 'B 1234 ASD', 'MBL', '08:49:31'),
('20170521002', '21/05/2017', 'Angga Fauzy', 'D 2345 QWE', 'MBL', '12:29:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
 ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `kendaraan`
--
ALTER TABLE `kendaraan`
 ADD PRIMARY KEY (`kode_jenis`);

--
-- Indexes for table `tb_parkir`
--
ALTER TABLE `tb_parkir`
 ADD PRIMARY KEY (`no_tiket`);

--
-- Indexes for table `tb_pkrkeluar`
--
ALTER TABLE `tb_pkrkeluar`
 ADD PRIMARY KEY (`no_tiket`);

--
-- Indexes for table `tb_pkrmasuk`
--
ALTER TABLE `tb_pkrmasuk`
 ADD PRIMARY KEY (`no_tiket`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
