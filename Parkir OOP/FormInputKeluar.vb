﻿Option Explicit On
Option Strict On
Imports MySql.Data.MySqlClient
Public Class FormInputKeluar
    Inherits System.Windows.Forms.Form
    Private myCam As iCam

    Private Sub FormInputKeluar_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
        LblTanggal.Text = Format(Now, "dddd, dd/MMMM/yyyy")
        myCam = New iCam
        Timer1.Start()

        Call Conn()
        koneksi.Open()
        Query = "select * from admin"
        CMD = New MySqlCommand(Query, koneksi)
        RD = CMD.ExecuteReader
        If RD.Read Then
            LblAdmin.Text = RD.Item("nama").ToString
        End If
        koneksi.Close()

        myCam.resetCam()
        myCam.initCam(Me.PictureBox1.Handle.ToInt32)
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        LblJam.Text = CStr(TimeOfDay)
    End Sub

    Private Sub TxtNoPolisi_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles TxtNoPolisi.KeyPress
        If e.KeyChar = ChrW(Keys.Enter) Then
            Dim cari As String

            cari = TxtNoPolisi.Text
            If cari = "" Then
                MsgBox("Data belum diinput", MsgBoxStyle.Exclamation, "Kesalahan")
            Else
                TxtTglKeluar.Text = Format(Now, "dd/MM/yyyy")
                TxtJamKeluar.Text = Format(Now, "HH:mm:ss")

                Call Conn()
                koneksi.Open()
                Query = "select * from tb_pkrmasuk where plat_nomor = '" & cari & "'"
                CMD = New MySqlCommand(Query, koneksi)
                RD = CMD.ExecuteReader
                If RD.Read = False Then
                    MsgBox("Data tidak ditemukan", MsgBoxStyle.Exclamation, "kesalahan")
                Else
                    LblNoTiket.Text = RD.Item("no_tiket").ToString
                    LblTglMasuk.Text = RD.Item("tgl_masuk").ToString
                    LblJamMasuk.Text = RD.Item("jam_masuk").ToString

                    If RD.Item("kode_jenis") Is "MTR" Then
                        LblJenis.Text = "MOTOR"
                        BacaTarif("MTR")
                    Else
                        LblJenis.Text = "MOBIL"
                        BacaTarif("MBL")
                    End If

                    Dim lama_hari As String = CStr(DateDiff(DateInterval.Day, CDate(LblTglMasuk.Text), CDate(TxtTglKeluar.Text)))
                    Dim lama_jam As String = CStr(DateDiff(DateInterval.Hour, CDate(LblJamMasuk.Text), CDate(TxtJamKeluar.Text)))
                    Dim lama_menit As String = CStr(DateDiff(DateInterval.Minute, CDate(LblJamMasuk.Text), CDate(TxtJamKeluar.Text)))
                    Dim lama_detik As String = CStr(DateDiff(DateInterval.Second, CDate(LblJamMasuk.Text), CDate(TxtJamKeluar.Text)))

                    Dim total_jamhari = CDbl(lama_hari) * 24
                    Dim lama_parkir As Integer = CInt(total_jamhari + CDbl(lama_jam))

                    If CDbl(lama_hari) > 0 Then
                        LblKetHari.Text = lama_hari
                        LblKetJam.Text = ""
                        LblKetHariJam.Text = lama_jam
                    Else
                        LblKetHari.Text = ""
                        LblKetJam.Text = "(" + lama_jam + ":" + lama_menit + ":" + lama_detik + ")"
                        LblKetHariJam.Text = ""
                    End If

                    LblLamaParkir.Text = CStr(lama_parkir)

                    Dim per_jam As Integer = CInt(lama_parkir)

                    If CInt(lama_hari) > 1 Then
                        LblBiayaParkir.Text = CStr(per_jam * Val(LblTarifJam.Text) + Val(LblTarifPertama.Text))
                    Else
                        LblBiayaParkir.Text = LblTarifPertama.Text
                        LblLamaParkir.Text = CStr(1)
                    End If
                End If
            End If
        End If
    End Sub

    Public Sub BacaTarif(ByVal kode As String)
        Call Conn()
        koneksi.Open()
        Query = "select * from kendaraan where kode_jenis = '" & kode & "'"
        CMD = New MySqlCommand(Query, koneksi)
        RD = CMD.ExecuteReader
        RD.Read()
        If RD.HasRows = False Then
            MsgBox("Data tarif tidak ditemukan", MsgBoxStyle.Exclamation, "kesalahan")
        Else
            LblTarifPertama.Text = RD.Item("tarif_parkir").ToString
            LblTarifJam.Text = RD.Item("tarif_perjam").ToString
            LblDenda.Text = RD.Item("denda").ToString
        End If
        koneksi.Close()
    End Sub

    Private Sub RadioBtnYa_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioBtnYa.CheckedChanged
        If RadioBtnYa.Checked = True Then
            LblDenda.Text = "0"
            TxtTotalBiaya.Text = LblBiayaParkir.Text
        End If
    End Sub

    Private Sub RadioBtnTidak_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioBtnTidak.CheckedChanged
        If RadioBtnTidak.Checked = True Then
            Call Conn()
            koneksi.Open()
            Query = "select * from kendaraan where jenis = '" & LblJenis.Text & "'"
            CMD = New MySqlCommand(Query, koneksi)
            RD = CMD.ExecuteReader
            RD.Read()
            If RD.HasRows Then
                LblDenda.Text = RD.Item("denda").ToString
                TxtTotalBiaya.Text = CStr(Val(LblBiayaParkir.Text) + Val(LblDenda.Text))
            End If
            koneksi.Close()
        End If
    End Sub

    Private Sub BtnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSimpan.Click

    End Sub

    Private Sub BtnKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnKeluar.Click
        MsgBox("Anda yakin keluar dari form ini ??", CType(MsgBoxStyle.Question + MsgBoxStyle.OkCancel, MsgBoxStyle))
        Me.Close()
    End Sub
End Class
