﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormInputMasuk
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LblAdmin = New System.Windows.Forms.Label()
        Me.LblTanggal = New System.Windows.Forms.Label()
        Me.LblJam = New System.Windows.Forms.Label()
        Me.Lbl1 = New System.Windows.Forms.Label()
        Me.Lbl2 = New System.Windows.Forms.Label()
        Me.Lbl3 = New System.Windows.Forms.Label()
        Me.TxtNoTiket = New System.Windows.Forms.TextBox()
        Me.TxtJamMasuk = New System.Windows.Forms.TextBox()
        Me.TxtTglMasuk = New System.Windows.Forms.TextBox()
        Me.Lbl4 = New System.Windows.Forms.Label()
        Me.TxtNoPolisi = New System.Windows.Forms.TextBox()
        Me.Lbl5 = New System.Windows.Forms.Label()
        Me.Lbl7 = New System.Windows.Forms.Label()
        Me.Lbl6 = New System.Windows.Forms.Label()
        Me.RadioBtnMotor = New System.Windows.Forms.RadioButton()
        Me.RadioBtnMobil = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BtnKeluar = New System.Windows.Forms.Button()
        Me.BtnBatal = New System.Windows.Forms.Button()
        Me.BtnInput = New System.Windows.Forms.Button()
        Me.BtnSimpan = New System.Windows.Forms.Button()
        Me.LblTarifPertama = New System.Windows.Forms.Label()
        Me.LblTarifPerJam = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(61, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(172, 31)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Parkir Masuk"
        '
        'LblAdmin
        '
        Me.LblAdmin.AutoSize = True
        Me.LblAdmin.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAdmin.Location = New System.Drawing.Point(61, 78)
        Me.LblAdmin.Name = "LblAdmin"
        Me.LblAdmin.Size = New System.Drawing.Size(90, 31)
        Me.LblAdmin.TabIndex = 1
        Me.LblAdmin.Text = "Admin"
        '
        'LblTanggal
        '
        Me.LblTanggal.AutoSize = True
        Me.LblTanggal.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTanggal.Location = New System.Drawing.Point(62, 116)
        Me.LblTanggal.Name = "LblTanggal"
        Me.LblTanggal.Size = New System.Drawing.Size(47, 25)
        Me.LblTanggal.TabIndex = 2
        Me.LblTanggal.Text = "N/A"
        '
        'LblJam
        '
        Me.LblJam.AutoSize = True
        Me.LblJam.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblJam.Location = New System.Drawing.Point(62, 146)
        Me.LblJam.Name = "LblJam"
        Me.LblJam.Size = New System.Drawing.Size(47, 25)
        Me.LblJam.TabIndex = 3
        Me.LblJam.Text = "N/A"
        '
        'Lbl1
        '
        Me.Lbl1.AutoSize = True
        Me.Lbl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl1.Location = New System.Drawing.Point(12, 204)
        Me.Lbl1.Name = "Lbl1"
        Me.Lbl1.Size = New System.Drawing.Size(67, 20)
        Me.Lbl1.TabIndex = 4
        Me.Lbl1.Text = "No Tiket"
        '
        'Lbl2
        '
        Me.Lbl2.AutoSize = True
        Me.Lbl2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl2.Location = New System.Drawing.Point(12, 234)
        Me.Lbl2.Name = "Lbl2"
        Me.Lbl2.Size = New System.Drawing.Size(117, 20)
        Me.Lbl2.TabIndex = 5
        Me.Lbl2.Text = "Tanggal Masuk"
        '
        'Lbl3
        '
        Me.Lbl3.AutoSize = True
        Me.Lbl3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl3.Location = New System.Drawing.Point(12, 266)
        Me.Lbl3.Name = "Lbl3"
        Me.Lbl3.Size = New System.Drawing.Size(90, 20)
        Me.Lbl3.TabIndex = 6
        Me.Lbl3.Text = "Jam Masuk"
        '
        'TxtNoTiket
        '
        Me.TxtNoTiket.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TxtNoTiket.Enabled = False
        Me.TxtNoTiket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtNoTiket.Location = New System.Drawing.Point(173, 202)
        Me.TxtNoTiket.Name = "TxtNoTiket"
        Me.TxtNoTiket.Size = New System.Drawing.Size(138, 22)
        Me.TxtNoTiket.TabIndex = 7
        '
        'TxtJamMasuk
        '
        Me.TxtJamMasuk.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TxtJamMasuk.Enabled = False
        Me.TxtJamMasuk.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtJamMasuk.Location = New System.Drawing.Point(173, 264)
        Me.TxtJamMasuk.Name = "TxtJamMasuk"
        Me.TxtJamMasuk.Size = New System.Drawing.Size(138, 22)
        Me.TxtJamMasuk.TabIndex = 8
        '
        'TxtTglMasuk
        '
        Me.TxtTglMasuk.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.TxtTglMasuk.Enabled = False
        Me.TxtTglMasuk.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtTglMasuk.Location = New System.Drawing.Point(173, 233)
        Me.TxtTglMasuk.Name = "TxtTglMasuk"
        Me.TxtTglMasuk.Size = New System.Drawing.Size(138, 22)
        Me.TxtTglMasuk.TabIndex = 9
        '
        'Lbl4
        '
        Me.Lbl4.AutoSize = True
        Me.Lbl4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl4.Location = New System.Drawing.Point(12, 314)
        Me.Lbl4.Name = "Lbl4"
        Me.Lbl4.Size = New System.Drawing.Size(69, 20)
        Me.Lbl4.TabIndex = 10
        Me.Lbl4.Text = "No Polisi"
        '
        'TxtNoPolisi
        '
        Me.TxtNoPolisi.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtNoPolisi.Location = New System.Drawing.Point(173, 311)
        Me.TxtNoPolisi.Name = "TxtNoPolisi"
        Me.TxtNoPolisi.Size = New System.Drawing.Size(138, 22)
        Me.TxtNoPolisi.TabIndex = 11
        '
        'Lbl5
        '
        Me.Lbl5.AutoSize = True
        Me.Lbl5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl5.Location = New System.Drawing.Point(12, 344)
        Me.Lbl5.Name = "Lbl5"
        Me.Lbl5.Size = New System.Drawing.Size(128, 20)
        Me.Lbl5.TabIndex = 12
        Me.Lbl5.Text = "Jenis Kendaraan"
        '
        'Lbl7
        '
        Me.Lbl7.AutoSize = True
        Me.Lbl7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl7.Location = New System.Drawing.Point(12, 403)
        Me.Lbl7.Name = "Lbl7"
        Me.Lbl7.Size = New System.Drawing.Size(111, 20)
        Me.Lbl7.TabIndex = 13
        Me.Lbl7.Text = "Tarif Per-Jam :"
        '
        'Lbl6
        '
        Me.Lbl6.AutoSize = True
        Me.Lbl6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl6.Location = New System.Drawing.Point(12, 374)
        Me.Lbl6.Name = "Lbl6"
        Me.Lbl6.Size = New System.Drawing.Size(146, 20)
        Me.Lbl6.TabIndex = 14
        Me.Lbl6.Text = "Tarif Jam Pertama :"
        '
        'RadioBtnMotor
        '
        Me.RadioBtnMotor.AutoSize = True
        Me.RadioBtnMotor.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioBtnMotor.Location = New System.Drawing.Point(173, 344)
        Me.RadioBtnMotor.Name = "RadioBtnMotor"
        Me.RadioBtnMotor.Size = New System.Drawing.Size(60, 20)
        Me.RadioBtnMotor.TabIndex = 15
        Me.RadioBtnMotor.TabStop = True
        Me.RadioBtnMotor.Text = "Motor"
        Me.RadioBtnMotor.UseVisualStyleBackColor = True
        '
        'RadioBtnMobil
        '
        Me.RadioBtnMobil.AutoSize = True
        Me.RadioBtnMobil.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioBtnMobil.Location = New System.Drawing.Point(252, 344)
        Me.RadioBtnMobil.Name = "RadioBtnMobil"
        Me.RadioBtnMobil.Size = New System.Drawing.Size(59, 20)
        Me.RadioBtnMobil.TabIndex = 16
        Me.RadioBtnMobil.TabStop = True
        Me.RadioBtnMobil.Text = "Mobil"
        Me.RadioBtnMobil.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BtnKeluar)
        Me.GroupBox1.Controls.Add(Me.BtnBatal)
        Me.GroupBox1.Controls.Add(Me.BtnInput)
        Me.GroupBox1.Controls.Add(Me.BtnSimpan)
        Me.GroupBox1.Location = New System.Drawing.Point(21, 457)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(530, 81)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        '
        'BtnKeluar
        '
        Me.BtnKeluar.Location = New System.Drawing.Point(404, 26)
        Me.BtnKeluar.Name = "BtnKeluar"
        Me.BtnKeluar.Size = New System.Drawing.Size(106, 38)
        Me.BtnKeluar.TabIndex = 25
        Me.BtnKeluar.Text = "Keluar"
        Me.BtnKeluar.UseVisualStyleBackColor = True
        '
        'BtnBatal
        '
        Me.BtnBatal.Location = New System.Drawing.Point(245, 26)
        Me.BtnBatal.Name = "BtnBatal"
        Me.BtnBatal.Size = New System.Drawing.Size(106, 38)
        Me.BtnBatal.TabIndex = 24
        Me.BtnBatal.Text = "Batal"
        Me.BtnBatal.UseVisualStyleBackColor = True
        '
        'BtnInput
        '
        Me.BtnInput.Location = New System.Drawing.Point(21, 26)
        Me.BtnInput.Name = "BtnInput"
        Me.BtnInput.Size = New System.Drawing.Size(106, 38)
        Me.BtnInput.TabIndex = 22
        Me.BtnInput.Text = "Input"
        Me.BtnInput.UseVisualStyleBackColor = True
        '
        'BtnSimpan
        '
        Me.BtnSimpan.Location = New System.Drawing.Point(133, 26)
        Me.BtnSimpan.Name = "BtnSimpan"
        Me.BtnSimpan.Size = New System.Drawing.Size(106, 38)
        Me.BtnSimpan.TabIndex = 23
        Me.BtnSimpan.Text = "Simpan"
        Me.BtnSimpan.UseVisualStyleBackColor = True
        '
        'LblTarifPertama
        '
        Me.LblTarifPertama.AutoSize = True
        Me.LblTarifPertama.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTarifPertama.Location = New System.Drawing.Point(169, 374)
        Me.LblTarifPertama.Name = "LblTarifPertama"
        Me.LblTarifPertama.Size = New System.Drawing.Size(18, 20)
        Me.LblTarifPertama.TabIndex = 22
        Me.LblTarifPertama.Text = "0"
        '
        'LblTarifPerJam
        '
        Me.LblTarifPerJam.AutoSize = True
        Me.LblTarifPerJam.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTarifPerJam.Location = New System.Drawing.Point(169, 403)
        Me.LblTarifPerJam.Name = "LblTarifPerJam"
        Me.LblTarifPerJam.Size = New System.Drawing.Size(18, 20)
        Me.LblTarifPerJam.TabIndex = 23
        Me.LblTarifPerJam.Text = "0"
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(316, 10)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(235, 169)
        Me.PictureBox1.TabIndex = 24
        Me.PictureBox1.TabStop = False
        '
        'Timer1
        '
        '
        'FormInputMasuk
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(577, 550)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.LblTarifPerJam)
        Me.Controls.Add(Me.LblTarifPertama)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.RadioBtnMobil)
        Me.Controls.Add(Me.RadioBtnMotor)
        Me.Controls.Add(Me.Lbl6)
        Me.Controls.Add(Me.Lbl7)
        Me.Controls.Add(Me.Lbl5)
        Me.Controls.Add(Me.TxtNoPolisi)
        Me.Controls.Add(Me.Lbl4)
        Me.Controls.Add(Me.TxtTglMasuk)
        Me.Controls.Add(Me.TxtJamMasuk)
        Me.Controls.Add(Me.TxtNoTiket)
        Me.Controls.Add(Me.Lbl3)
        Me.Controls.Add(Me.Lbl2)
        Me.Controls.Add(Me.Lbl1)
        Me.Controls.Add(Me.LblJam)
        Me.Controls.Add(Me.LblTanggal)
        Me.Controls.Add(Me.LblAdmin)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FormInputMasuk"
        Me.Text = "Form Input Masuk"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LblAdmin As System.Windows.Forms.Label
    Friend WithEvents LblTanggal As System.Windows.Forms.Label
    Friend WithEvents LblJam As System.Windows.Forms.Label
    Friend WithEvents Lbl1 As System.Windows.Forms.Label
    Friend WithEvents Lbl2 As System.Windows.Forms.Label
    Friend WithEvents Lbl3 As System.Windows.Forms.Label
    Friend WithEvents TxtNoTiket As System.Windows.Forms.TextBox
    Friend WithEvents TxtJamMasuk As System.Windows.Forms.TextBox
    Friend WithEvents TxtTglMasuk As System.Windows.Forms.TextBox
    Friend WithEvents Lbl4 As System.Windows.Forms.Label
    Friend WithEvents TxtNoPolisi As System.Windows.Forms.TextBox
    Friend WithEvents Lbl5 As System.Windows.Forms.Label
    Friend WithEvents Lbl7 As System.Windows.Forms.Label
    Friend WithEvents Lbl6 As System.Windows.Forms.Label
    Friend WithEvents RadioBtnMotor As System.Windows.Forms.RadioButton
    Friend WithEvents RadioBtnMobil As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BtnKeluar As System.Windows.Forms.Button
    Friend WithEvents BtnBatal As System.Windows.Forms.Button
    Friend WithEvents BtnInput As System.Windows.Forms.Button
    Friend WithEvents BtnSimpan As System.Windows.Forms.Button
    Friend WithEvents LblTarifPertama As System.Windows.Forms.Label
    Friend WithEvents LblTarifPerJam As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer

End Class
