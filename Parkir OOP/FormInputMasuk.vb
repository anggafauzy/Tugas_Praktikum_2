﻿Option Explicit On
Option Strict On
Imports MySql.Data.MySqlClient
Public Class FormInputMasuk
    Inherits System.Windows.Forms.Form
    Private myCam As iCam

    Sub BuatKode()
        'pola = XXXDDMMYY000
        'contoh = MTR120517001
        Call Conn()
        koneksi.Open()

        Dim NoLama As String = ""
        Dim NoBaru As String = ""
        Dim NoAwal As String = Format(Now, "yyyyMMdd")

        Query = "Select * from tb_pkrmasuk Order By no_tiket DESC"
        CMD = New MySqlCommand(Query, koneksi)
        RD = CMD.ExecuteReader

        If RD.Read Then
            NoLama = Mid(CStr(RD.Item("no_tiket")), 9, 3)
        Else
            TxtNoTiket.Text = NoAwal & "001"
            Exit Sub
        End If
        NoBaru = CStr(Val(NoLama) + 1)
        TxtNoTiket.Text = NoAwal & Mid("000", 1, 3 - NoBaru.Length) & NoBaru
    End Sub

    Private Sub FormInputMasuk_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.PictureBox1.SizeMode = PictureBoxSizeMode.StretchImage
        LblTanggal.Text = Format(Now, "dddd, dd/MMMM/yyyy")
        myCam = New iCam
        Timer1.Start()

        Call Conn()
        koneksi.Open()
        Query = "select * from admin"
        CMD = New MySqlCommand(Query, koneksi)
        RD = CMD.ExecuteReader
        If RD.Read Then
            LblAdmin.Text = RD.Item("nama").ToString
        End If
    End Sub

    Private Sub BtnInput_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnInput.Click
        myCam.resetCam()
        myCam.initCam(Me.PictureBox1.Handle.ToInt32)

        BuatKode()
        TxtTglMasuk.Text = Format(Now, "dd/MM/yyyy")
        TxtJamMasuk.Text = Format(Now, "HH:mm:ss")
    End Sub

    Private Sub BtnBatal_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnBatal.Click
        myCam.closeCam()
    End Sub

    Private Sub BtnKeluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnKeluar.Click
        myCam.closeCam()
        Application.DoEvents()
        myCam = Nothing
        Close()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        LblJam.Text = CStr(TimeOfDay)
    End Sub

    Private Sub RadioBtnMotor_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioBtnMotor.CheckedChanged
        BuatKode()
        Call Conn()
        koneksi.Open()
        Query = "select * from kendaraan where kode_jenis='MTR'"
        CMD = New MySqlCommand(Query, koneksi)
        RD = CMD.ExecuteReader
        If RD.Read Then
            'TxtNoTiket.Text = RD.Item("kode_jenis").ToString & TxtNoTiket.Text
            LblTarifPertama.Text = RD.Item("tarif_parkir").ToString
            LblTarifPerJam.Text = RD.Item("tarif_perjam").ToString
        End If
    End Sub

    Private Sub RadioBtnMobil_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioBtnMobil.CheckedChanged
        BuatKode()
        Call Conn()
        koneksi.Open()
        Query = "select * from kendaraan where kode_jenis='MBL'"
        CMD = New MySqlCommand(Query, koneksi)
        RD = CMD.ExecuteReader
        If RD.Read Then
            'TxtNoTiket.Text = RD.Item("kode_jenis").ToString & TxtNoTiket.Text
            LblTarifPertama.Text = RD.Item("tarif_parkir").ToString
            LblTarifPerJam.Text = RD.Item("tarif_perjam").ToString
        End If
    End Sub

    Private Sub BtnSimpan_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles BtnSimpan.Click
        Call Conn()
        koneksi.Open()
        'Query = "select * from tb_pkrmasuk"
        'CMD = New MySqlCommand(Query, koneksi)
        'RD = CMD.ExecuteReader
        Try
            If RadioBtnMobil.Checked = True Then
                Dim mbl As String = "MBL"
                Query = "insert into tb_pkrmasuk (no_tiket, tgl_masuk, nama_admin, plat_nomor, kode_jenis, jam_masuk)" & _
                            "Values ('" & TxtNoTiket.Text & "','" & TxtTglMasuk.Text & "','" & LblAdmin.Text & "','" & TxtNoPolisi.Text & "','" & mbl & "','" & TxtJamMasuk.Text & "')"
                CMD = New MySqlCommand(Query, koneksi)
                CMD.ExecuteNonQuery()
                MsgBox("Data Baru tersimpan", MsgBoxStyle.Information)
            Else
                Dim mtr As String = "MTR"
                Query = "insert into tb_pkrmasuk (no_tiket, tgl_masuk, nama_admin, plat_nomor, kode_jenis, jam_masuk)" & _
                            "Values ('" & TxtNoTiket.Text & "','" & TxtTglMasuk.Text & "','" & LblAdmin.Text & "','" & TxtNoPolisi.Text & "','" & mtr & "','" & TxtJamMasuk.Text & "')"
                CMD = New MySqlCommand(Query, koneksi)
                CMD.ExecuteNonQuery()
                MsgBox("Data Baru tersimpan", MsgBoxStyle.Information)
            End If
        Catch ex As MySqlException
            MessageBox.Show("Error" & ex.Message)
        End Try
        koneksi.Close()
    End Sub
End Class
