﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormInputKeluar
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.LblAdmin = New System.Windows.Forms.Label()
        Me.LblTanggal = New System.Windows.Forms.Label()
        Me.LblJam = New System.Windows.Forms.Label()
        Me.Lbl1 = New System.Windows.Forms.Label()
        Me.Lbl2 = New System.Windows.Forms.Label()
        Me.Lbl3 = New System.Windows.Forms.Label()
        Me.TxtJamKeluar = New System.Windows.Forms.TextBox()
        Me.TxtTglKeluar = New System.Windows.Forms.TextBox()
        Me.Lbl4 = New System.Windows.Forms.Label()
        Me.TxtNoPolisi = New System.Windows.Forms.TextBox()
        Me.Lbl5 = New System.Windows.Forms.Label()
        Me.Lbl7 = New System.Windows.Forms.Label()
        Me.Lbl6 = New System.Windows.Forms.Label()
        Me.RadioBtnYa = New System.Windows.Forms.RadioButton()
        Me.RadioBtnTidak = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BtnKeluar = New System.Windows.Forms.Button()
        Me.BtnSimpan = New System.Windows.Forms.Button()
        Me.LblTarifPertama = New System.Windows.Forms.Label()
        Me.LblTarifJam = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.TxtTotalBiaya = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.LblDenda = New System.Windows.Forms.Label()
        Me.LblLamaParkir = New System.Windows.Forms.Label()
        Me.LblKetHari = New System.Windows.Forms.Label()
        Me.LblKetHariJam = New System.Windows.Forms.Label()
        Me.LblBiayaParkir = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.LblJenis = New System.Windows.Forms.Label()
        Me.LblJamMasuk = New System.Windows.Forms.Label()
        Me.LblNoTiket = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.LblTglMasuk = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.LblKetJam = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(61, 21)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(170, 31)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Parkir Keluar"
        '
        'LblAdmin
        '
        Me.LblAdmin.AutoSize = True
        Me.LblAdmin.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblAdmin.Location = New System.Drawing.Point(61, 78)
        Me.LblAdmin.Name = "LblAdmin"
        Me.LblAdmin.Size = New System.Drawing.Size(90, 31)
        Me.LblAdmin.TabIndex = 1
        Me.LblAdmin.Text = "Admin"
        '
        'LblTanggal
        '
        Me.LblTanggal.AutoSize = True
        Me.LblTanggal.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTanggal.Location = New System.Drawing.Point(62, 116)
        Me.LblTanggal.Name = "LblTanggal"
        Me.LblTanggal.Size = New System.Drawing.Size(47, 25)
        Me.LblTanggal.TabIndex = 2
        Me.LblTanggal.Text = "N/A"
        '
        'LblJam
        '
        Me.LblJam.AutoSize = True
        Me.LblJam.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblJam.Location = New System.Drawing.Point(62, 146)
        Me.LblJam.Name = "LblJam"
        Me.LblJam.Size = New System.Drawing.Size(47, 25)
        Me.LblJam.TabIndex = 3
        Me.LblJam.Text = "N/A"
        '
        'Lbl1
        '
        Me.Lbl1.AutoSize = True
        Me.Lbl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl1.Location = New System.Drawing.Point(12, 202)
        Me.Lbl1.Name = "Lbl1"
        Me.Lbl1.Size = New System.Drawing.Size(69, 16)
        Me.Lbl1.TabIndex = 4
        Me.Lbl1.Text = "No Tiket"
        '
        'Lbl2
        '
        Me.Lbl2.AutoSize = True
        Me.Lbl2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl2.Location = New System.Drawing.Point(304, 225)
        Me.Lbl2.Name = "Lbl2"
        Me.Lbl2.Size = New System.Drawing.Size(100, 16)
        Me.Lbl2.TabIndex = 5
        Me.Lbl2.Text = "Tanggal Keluar"
        '
        'Lbl3
        '
        Me.Lbl3.AutoSize = True
        Me.Lbl3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl3.Location = New System.Drawing.Point(305, 249)
        Me.Lbl3.Name = "Lbl3"
        Me.Lbl3.Size = New System.Drawing.Size(75, 16)
        Me.Lbl3.TabIndex = 6
        Me.Lbl3.Text = "Jam Keluar"
        '
        'TxtJamKeluar
        '
        Me.TxtJamKeluar.Enabled = False
        Me.TxtJamKeluar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtJamKeluar.Location = New System.Drawing.Point(424, 246)
        Me.TxtJamKeluar.Name = "TxtJamKeluar"
        Me.TxtJamKeluar.Size = New System.Drawing.Size(127, 20)
        Me.TxtJamKeluar.TabIndex = 8
        '
        'TxtTglKeluar
        '
        Me.TxtTglKeluar.Enabled = False
        Me.TxtTglKeluar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtTglKeluar.Location = New System.Drawing.Point(424, 222)
        Me.TxtTglKeluar.Name = "TxtTglKeluar"
        Me.TxtTglKeluar.Size = New System.Drawing.Size(127, 20)
        Me.TxtTglKeluar.TabIndex = 9
        '
        'Lbl4
        '
        Me.Lbl4.AutoSize = True
        Me.Lbl4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl4.Location = New System.Drawing.Point(12, 371)
        Me.Lbl4.Name = "Lbl4"
        Me.Lbl4.Size = New System.Drawing.Size(62, 16)
        Me.Lbl4.TabIndex = 10
        Me.Lbl4.Text = "No Polisi"
        '
        'TxtNoPolisi
        '
        Me.TxtNoPolisi.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtNoPolisi.Location = New System.Drawing.Point(173, 368)
        Me.TxtNoPolisi.Name = "TxtNoPolisi"
        Me.TxtNoPolisi.Size = New System.Drawing.Size(138, 20)
        Me.TxtNoPolisi.TabIndex = 11
        '
        'Lbl5
        '
        Me.Lbl5.AutoSize = True
        Me.Lbl5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl5.Location = New System.Drawing.Point(12, 398)
        Me.Lbl5.Name = "Lbl5"
        Me.Lbl5.Size = New System.Drawing.Size(76, 16)
        Me.Lbl5.TabIndex = 12
        Me.Lbl5.Text = "Ada Struk ?"
        '
        'Lbl7
        '
        Me.Lbl7.AutoSize = True
        Me.Lbl7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl7.Location = New System.Drawing.Point(12, 342)
        Me.Lbl7.Name = "Lbl7"
        Me.Lbl7.Size = New System.Drawing.Size(95, 16)
        Me.Lbl7.TabIndex = 13
        Me.Lbl7.Text = "Tarif Per-Jam :"
        '
        'Lbl6
        '
        Me.Lbl6.AutoSize = True
        Me.Lbl6.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Lbl6.Location = New System.Drawing.Point(12, 318)
        Me.Lbl6.Name = "Lbl6"
        Me.Lbl6.Size = New System.Drawing.Size(124, 16)
        Me.Lbl6.TabIndex = 14
        Me.Lbl6.Text = "Tarif Jam Pertama :"
        '
        'RadioBtnYa
        '
        Me.RadioBtnYa.AutoSize = True
        Me.RadioBtnYa.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioBtnYa.Location = New System.Drawing.Point(173, 398)
        Me.RadioBtnYa.Name = "RadioBtnYa"
        Me.RadioBtnYa.Size = New System.Drawing.Size(43, 20)
        Me.RadioBtnYa.TabIndex = 15
        Me.RadioBtnYa.TabStop = True
        Me.RadioBtnYa.Text = "Ya"
        Me.RadioBtnYa.UseVisualStyleBackColor = True
        '
        'RadioBtnTidak
        '
        Me.RadioBtnTidak.AutoSize = True
        Me.RadioBtnTidak.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioBtnTidak.Location = New System.Drawing.Point(252, 398)
        Me.RadioBtnTidak.Name = "RadioBtnTidak"
        Me.RadioBtnTidak.Size = New System.Drawing.Size(61, 20)
        Me.RadioBtnTidak.TabIndex = 16
        Me.RadioBtnTidak.TabStop = True
        Me.RadioBtnTidak.Text = "Tidak"
        Me.RadioBtnTidak.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BtnKeluar)
        Me.GroupBox1.Controls.Add(Me.BtnSimpan)
        Me.GroupBox1.Location = New System.Drawing.Point(21, 484)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(530, 59)
        Me.GroupBox1.TabIndex = 21
        Me.GroupBox1.TabStop = False
        '
        'BtnKeluar
        '
        Me.BtnKeluar.Location = New System.Drawing.Point(404, 14)
        Me.BtnKeluar.Name = "BtnKeluar"
        Me.BtnKeluar.Size = New System.Drawing.Size(106, 38)
        Me.BtnKeluar.TabIndex = 25
        Me.BtnKeluar.Text = "Keluar"
        Me.BtnKeluar.UseVisualStyleBackColor = True
        '
        'BtnSimpan
        '
        Me.BtnSimpan.Location = New System.Drawing.Point(21, 14)
        Me.BtnSimpan.Name = "BtnSimpan"
        Me.BtnSimpan.Size = New System.Drawing.Size(106, 38)
        Me.BtnSimpan.TabIndex = 23
        Me.BtnSimpan.Text = "Simpan"
        Me.BtnSimpan.UseVisualStyleBackColor = True
        '
        'LblTarifPertama
        '
        Me.LblTarifPertama.AutoSize = True
        Me.LblTarifPertama.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTarifPertama.Location = New System.Drawing.Point(169, 318)
        Me.LblTarifPertama.Name = "LblTarifPertama"
        Me.LblTarifPertama.Size = New System.Drawing.Size(15, 16)
        Me.LblTarifPertama.TabIndex = 22
        Me.LblTarifPertama.Text = "0"
        '
        'LblTarifJam
        '
        Me.LblTarifJam.AutoSize = True
        Me.LblTarifJam.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTarifJam.Location = New System.Drawing.Point(169, 342)
        Me.LblTarifJam.Name = "LblTarifJam"
        Me.LblTarifJam.Size = New System.Drawing.Size(15, 16)
        Me.LblTarifJam.TabIndex = 23
        Me.LblTarifJam.Text = "0"
        '
        'PictureBox1
        '
        Me.PictureBox1.Location = New System.Drawing.Point(316, 10)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(235, 169)
        Me.PictureBox1.TabIndex = 24
        Me.PictureBox1.TabStop = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 249)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(77, 16)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Jam Masuk"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(12, 273)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(80, 16)
        Me.Label4.TabIndex = 26
        Me.Label4.Text = "Lama Parkir"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(199, 273)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(34, 16)
        Me.Label5.TabIndex = 27
        Me.Label5.Text = "Jam"
        '
        'TxtTotalBiaya
        '
        Me.TxtTotalBiaya.Enabled = False
        Me.TxtTotalBiaya.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TxtTotalBiaya.Location = New System.Drawing.Point(174, 426)
        Me.TxtTotalBiaya.Multiline = True
        Me.TxtTotalBiaya.Name = "TxtTotalBiaya"
        Me.TxtTotalBiaya.Size = New System.Drawing.Size(138, 51)
        Me.TxtTotalBiaya.TabIndex = 28
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(11, 426)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(101, 24)
        Me.Label6.TabIndex = 29
        Me.Label6.Text = "Total Biaya"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(355, 400)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(49, 16)
        Me.Label7.TabIndex = 30
        Me.Label7.Text = "Denda"
        '
        'LblDenda
        '
        Me.LblDenda.AutoSize = True
        Me.LblDenda.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblDenda.Location = New System.Drawing.Point(447, 400)
        Me.LblDenda.Name = "LblDenda"
        Me.LblDenda.Size = New System.Drawing.Size(15, 16)
        Me.LblDenda.TabIndex = 31
        Me.LblDenda.Text = "0"
        '
        'LblLamaParkir
        '
        Me.LblLamaParkir.AutoSize = True
        Me.LblLamaParkir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblLamaParkir.Location = New System.Drawing.Point(169, 273)
        Me.LblLamaParkir.Name = "LblLamaParkir"
        Me.LblLamaParkir.Size = New System.Drawing.Size(31, 16)
        Me.LblLamaParkir.TabIndex = 33
        Me.LblLamaParkir.Text = "N/A"
        '
        'LblKetHari
        '
        Me.LblKetHari.AutoSize = True
        Me.LblKetHari.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblKetHari.Location = New System.Drawing.Point(177, 294)
        Me.LblKetHari.Name = "LblKetHari"
        Me.LblKetHari.Size = New System.Drawing.Size(31, 16)
        Me.LblKetHari.TabIndex = 34
        Me.LblKetHari.Text = "N/A"
        '
        'LblKetHariJam
        '
        Me.LblKetHariJam.AutoSize = True
        Me.LblKetHariJam.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblKetHariJam.Location = New System.Drawing.Point(243, 294)
        Me.LblKetHariJam.Name = "LblKetHariJam"
        Me.LblKetHariJam.Size = New System.Drawing.Size(31, 16)
        Me.LblKetHariJam.TabIndex = 35
        Me.LblKetHariJam.Text = "N/A"
        '
        'LblBiayaParkir
        '
        Me.LblBiayaParkir.AutoSize = True
        Me.LblBiayaParkir.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblBiayaParkir.Location = New System.Drawing.Point(305, 318)
        Me.LblBiayaParkir.Name = "LblBiayaParkir"
        Me.LblBiayaParkir.Size = New System.Drawing.Size(15, 16)
        Me.LblBiayaParkir.TabIndex = 37
        Me.LblBiayaParkir.Text = "0"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(211, 318)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(81, 16)
        Me.Label12.TabIndex = 36
        Me.Label12.Text = "Biaya Parkir"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.Location = New System.Drawing.Point(305, 202)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(109, 16)
        Me.Label13.TabIndex = 38
        Me.Label13.Text = "Jenis Kendaraan"
        '
        'LblJenis
        '
        Me.LblJenis.AutoSize = True
        Me.LblJenis.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblJenis.Location = New System.Drawing.Point(422, 202)
        Me.LblJenis.Name = "LblJenis"
        Me.LblJenis.Size = New System.Drawing.Size(31, 16)
        Me.LblJenis.TabIndex = 39
        Me.LblJenis.Text = "N/A"
        '
        'LblJamMasuk
        '
        Me.LblJamMasuk.AutoSize = True
        Me.LblJamMasuk.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblJamMasuk.Location = New System.Drawing.Point(169, 249)
        Me.LblJamMasuk.Name = "LblJamMasuk"
        Me.LblJamMasuk.Size = New System.Drawing.Size(31, 16)
        Me.LblJamMasuk.TabIndex = 40
        Me.LblJamMasuk.Text = "N/A"
        '
        'LblNoTiket
        '
        Me.LblNoTiket.AutoSize = True
        Me.LblNoTiket.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblNoTiket.Location = New System.Drawing.Point(169, 202)
        Me.LblNoTiket.Name = "LblNoTiket"
        Me.LblNoTiket.Size = New System.Drawing.Size(36, 16)
        Me.LblNoTiket.TabIndex = 41
        Me.LblNoTiket.Text = "N/A"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label16.Location = New System.Drawing.Point(11, 225)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(102, 16)
        Me.Label16.TabIndex = 42
        Me.Label16.Text = "Tanggal Masuk"
        '
        'LblTglMasuk
        '
        Me.LblTglMasuk.AutoSize = True
        Me.LblTglMasuk.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblTglMasuk.Location = New System.Drawing.Point(169, 225)
        Me.LblTglMasuk.Name = "LblTglMasuk"
        Me.LblTglMasuk.Size = New System.Drawing.Size(31, 16)
        Me.LblTglMasuk.TabIndex = 43
        Me.LblTglMasuk.Text = "N/A"
        '
        'Timer1
        '
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(206, 294)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(33, 16)
        Me.Label2.TabIndex = 44
        Me.Label2.Text = "Hari"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(264, 294)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(38, 16)
        Me.Label8.TabIndex = 45
        Me.Label8.Text = "Jam)"
        '
        'LblKetJam
        '
        Me.LblKetJam.AutoSize = True
        Me.LblKetJam.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LblKetJam.Location = New System.Drawing.Point(264, 273)
        Me.LblKetJam.Name = "LblKetJam"
        Me.LblKetJam.Size = New System.Drawing.Size(31, 16)
        Me.LblKetJam.TabIndex = 32
        Me.LblKetJam.Text = "N/A"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(169, 293)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(12, 16)
        Me.Label9.TabIndex = 46
        Me.Label9.Text = "("
        '
        'FormInputKeluar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(577, 550)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.LblTglMasuk)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.LblNoTiket)
        Me.Controls.Add(Me.LblJamMasuk)
        Me.Controls.Add(Me.LblJenis)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.LblBiayaParkir)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.LblKetHariJam)
        Me.Controls.Add(Me.LblKetHari)
        Me.Controls.Add(Me.LblLamaParkir)
        Me.Controls.Add(Me.LblKetJam)
        Me.Controls.Add(Me.LblDenda)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.TxtTotalBiaya)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.LblTarifJam)
        Me.Controls.Add(Me.LblTarifPertama)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.RadioBtnTidak)
        Me.Controls.Add(Me.RadioBtnYa)
        Me.Controls.Add(Me.Lbl6)
        Me.Controls.Add(Me.Lbl7)
        Me.Controls.Add(Me.Lbl5)
        Me.Controls.Add(Me.TxtNoPolisi)
        Me.Controls.Add(Me.Lbl4)
        Me.Controls.Add(Me.TxtTglKeluar)
        Me.Controls.Add(Me.TxtJamKeluar)
        Me.Controls.Add(Me.Lbl3)
        Me.Controls.Add(Me.Lbl2)
        Me.Controls.Add(Me.Lbl1)
        Me.Controls.Add(Me.LblJam)
        Me.Controls.Add(Me.LblTanggal)
        Me.Controls.Add(Me.LblAdmin)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FormInputKeluar"
        Me.Text = "Form Input Keluar"
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents LblAdmin As System.Windows.Forms.Label
    Friend WithEvents LblTanggal As System.Windows.Forms.Label
    Friend WithEvents LblJam As System.Windows.Forms.Label
    Friend WithEvents Lbl1 As System.Windows.Forms.Label
    Friend WithEvents Lbl2 As System.Windows.Forms.Label
    Friend WithEvents Lbl3 As System.Windows.Forms.Label
    Friend WithEvents TxtJamKeluar As System.Windows.Forms.TextBox
    Friend WithEvents TxtTglKeluar As System.Windows.Forms.TextBox
    Friend WithEvents Lbl4 As System.Windows.Forms.Label
    Friend WithEvents TxtNoPolisi As System.Windows.Forms.TextBox
    Friend WithEvents Lbl5 As System.Windows.Forms.Label
    Friend WithEvents Lbl7 As System.Windows.Forms.Label
    Friend WithEvents Lbl6 As System.Windows.Forms.Label
    Friend WithEvents RadioBtnYa As System.Windows.Forms.RadioButton
    Friend WithEvents RadioBtnTidak As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents BtnKeluar As System.Windows.Forms.Button
    Friend WithEvents BtnSimpan As System.Windows.Forms.Button
    Friend WithEvents LblTarifPertama As System.Windows.Forms.Label
    Friend WithEvents LblTarifJam As System.Windows.Forms.Label
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents TxtTotalBiaya As System.Windows.Forms.TextBox
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents LblDenda As System.Windows.Forms.Label
    Friend WithEvents LblLamaParkir As System.Windows.Forms.Label
    Friend WithEvents LblKetHari As System.Windows.Forms.Label
    Friend WithEvents LblKetHariJam As System.Windows.Forms.Label
    Friend WithEvents LblBiayaParkir As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents LblJenis As System.Windows.Forms.Label
    Friend WithEvents LblJamMasuk As System.Windows.Forms.Label
    Friend WithEvents LblNoTiket As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents LblTglMasuk As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents LblKetJam As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label

End Class
